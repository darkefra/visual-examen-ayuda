﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pricipal_empresa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ADM_Click(object sender, EventArgs e)
        {
            ADMINISTRADORES ADM = new ADMINISTRADORES();
            ADM.Show();

        }

        private void EMPPH_Click(object sender, EventArgs e)
        {
            EMPLEADOS_POR_HORA EMPPH = new EMPLEADOS_POR_HORA();
             EMPPH.Show();
        }

        private void EMPPC_Click(object sender, EventArgs e)
        {
            EMPLEADOS_POR_COMISION EMPPC = new EMPLEADOS_POR_COMISION();
            EMPPC.Show();
        }

        private void EMPPD_Click(object sender, EventArgs e)
        {
            EMPLEADO_POR_DESTAJO EMPPD = new EMPLEADO_POR_DESTAJO();
            EMPPD.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show(" Bienvenido empleado a la calculadora de salario ");
        }
    }
}
