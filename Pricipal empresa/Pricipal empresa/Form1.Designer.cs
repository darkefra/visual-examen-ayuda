﻿namespace Pricipal_empresa
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ADM = new System.Windows.Forms.Button();
            this.EMPPH = new System.Windows.Forms.Button();
            this.EMPPC = new System.Windows.Forms.Button();
            this.EMPPD = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(89, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(497, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipos de empleados que trabajan en foxconn";
            // 
            // ADM
            // 
            this.ADM.Location = new System.Drawing.Point(94, 106);
            this.ADM.Name = "ADM";
            this.ADM.Size = new System.Drawing.Size(152, 58);
            this.ADM.TabIndex = 1;
            this.ADM.Text = "ADMINISTRADORES";
            this.ADM.UseVisualStyleBackColor = true;
            this.ADM.Click += new System.EventHandler(this.ADM_Click);
            // 
            // EMPPH
            // 
            this.EMPPH.Location = new System.Drawing.Point(436, 106);
            this.EMPPH.Name = "EMPPH";
            this.EMPPH.Size = new System.Drawing.Size(150, 58);
            this.EMPPH.TabIndex = 2;
            this.EMPPH.Text = "EMPLEADOS POR HORA";
            this.EMPPH.UseVisualStyleBackColor = true;
            this.EMPPH.Click += new System.EventHandler(this.EMPPH_Click);
            // 
            // EMPPC
            // 
            this.EMPPC.Location = new System.Drawing.Point(94, 228);
            this.EMPPC.Name = "EMPPC";
            this.EMPPC.Size = new System.Drawing.Size(152, 61);
            this.EMPPC.TabIndex = 3;
            this.EMPPC.Text = "EMPLEADOS POR COMISION";
            this.EMPPC.UseVisualStyleBackColor = true;
            this.EMPPC.Click += new System.EventHandler(this.EMPPC_Click);
            // 
            // EMPPD
            // 
            this.EMPPD.Location = new System.Drawing.Point(436, 228);
            this.EMPPD.Name = "EMPPD";
            this.EMPPD.Size = new System.Drawing.Size(150, 61);
            this.EMPPD.TabIndex = 4;
            this.EMPPD.Text = "EMPLEADOS POR DESTAJO";
            this.EMPPD.UseVisualStyleBackColor = true;
            this.EMPPD.Click += new System.EventHandler(this.EMPPD_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Pricipal_empresa.Properties.Resources.fox;
            this.ClientSize = new System.Drawing.Size(679, 414);
            this.Controls.Add(this.EMPPD);
            this.Controls.Add(this.EMPPC);
            this.Controls.Add(this.EMPPH);
            this.Controls.Add(this.ADM);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Empresas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ADM;
        private System.Windows.Forms.Button EMPPH;
        private System.Windows.Forms.Button EMPPC;
        private System.Windows.Forms.Button EMPPD;
    }
}

