﻿namespace Pricipal_empresa
{
    partial class EMPLEADO_POR_DESTAJO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.BT2 = new System.Windows.Forms.Button();
            this.TEX8 = new System.Windows.Forms.TextBox();
            this.TEX7 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "VENTANA EMPLEADOS POR DESTAJO";
            // 
            // BT2
            // 
            this.BT2.Location = new System.Drawing.Point(75, 176);
            this.BT2.Name = "BT2";
            this.BT2.Size = new System.Drawing.Size(144, 23);
            this.BT2.TabIndex = 15;
            this.BT2.Text = "Calcular paga";
            this.BT2.UseVisualStyleBackColor = true;
            this.BT2.Click += new System.EventHandler(this.BT2_Click);
            // 
            // TEX8
            // 
            this.TEX8.Location = new System.Drawing.Point(167, 65);
            this.TEX8.Name = "TEX8";
            this.TEX8.Size = new System.Drawing.Size(100, 20);
            this.TEX8.TabIndex = 14;
            // 
            // TEX7
            // 
            this.TEX7.Location = new System.Drawing.Point(167, 124);
            this.TEX7.Name = "TEX7";
            this.TEX7.Size = new System.Drawing.Size(100, 20);
            this.TEX7.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Piezas producidas ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Sueldo por pieza";
            // 
            // EMPLEADO_POR_DESTAJO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.BT2);
            this.Controls.Add(this.TEX8);
            this.Controls.Add(this.TEX7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EMPLEADO_POR_DESTAJO";
            this.Text = "EMPLEADO_POR_DESTAJO";
            this.Load += new System.EventHandler(this.EMPLEADO_POR_DESTAJO_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BT2;
        private System.Windows.Forms.TextBox TEX8;
        private System.Windows.Forms.TextBox TEX7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}