﻿namespace Pricipal_empresa
{
    partial class ADMINISTRADORES
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TEX2 = new System.Windows.Forms.TextBox();
            this.TEX1 = new System.Windows.Forms.TextBox();
            this.BT1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "VENTANA ADMINISTRADORES ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Saldo por hora";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Horas trabajadas";
            // 
            // TEX2
            // 
            this.TEX2.Location = new System.Drawing.Point(172, 149);
            this.TEX2.Name = "TEX2";
            this.TEX2.Size = new System.Drawing.Size(100, 20);
            this.TEX2.TabIndex = 3;
            // 
            // TEX1
            // 
            this.TEX1.Location = new System.Drawing.Point(172, 90);
            this.TEX1.Name = "TEX1";
            this.TEX1.Size = new System.Drawing.Size(100, 20);
            this.TEX1.TabIndex = 4;
            // 
            // BT1
            // 
            this.BT1.Location = new System.Drawing.Point(80, 201);
            this.BT1.Name = "BT1";
            this.BT1.Size = new System.Drawing.Size(144, 23);
            this.BT1.TabIndex = 5;
            this.BT1.Text = "Calcular paga";
            this.BT1.UseVisualStyleBackColor = true;
            this.BT1.Click += new System.EventHandler(this.BT1_Click);
            // 
            // ADMINISTRADORES
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 262);
            this.Controls.Add(this.BT1);
            this.Controls.Add(this.TEX1);
            this.Controls.Add(this.TEX2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ADMINISTRADORES";
            this.Text = "ADMINISTRADORES";
            this.Load += new System.EventHandler(this.ADMINISTRADORES_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TEX2;
        private System.Windows.Forms.TextBox TEX1;
        private System.Windows.Forms.Button BT1;
    }
}