﻿namespace Pricipal_empresa
{
    partial class EMPLEADOS_POR_COMISION
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.BT2 = new System.Windows.Forms.Button();
            this.TEX5 = new System.Windows.Forms.TextBox();
            this.TEX6 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "VENTANA EMPLEADOS POR COMISION";
            // 
            // BT2
            // 
            this.BT2.Location = new System.Drawing.Point(107, 188);
            this.BT2.Name = "BT2";
            this.BT2.Size = new System.Drawing.Size(144, 23);
            this.BT2.TabIndex = 15;
            this.BT2.Text = "Calcular paga";
            this.BT2.UseVisualStyleBackColor = true;
            this.BT2.Click += new System.EventHandler(this.BT2_Click);
            // 
            // TEX5
            // 
            this.TEX5.Location = new System.Drawing.Point(225, 67);
            this.TEX5.Name = "TEX5";
            this.TEX5.Size = new System.Drawing.Size(100, 20);
            this.TEX5.TabIndex = 14;
            // 
            // TEX6
            // 
            this.TEX6.Location = new System.Drawing.Point(225, 124);
            this.TEX6.Name = "TEX6";
            this.TEX6.Size = new System.Drawing.Size(100, 20);
            this.TEX6.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(71, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Horas trabajadas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Ventas brutas mensuales";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // EMPLEADOS_POR_COMISION
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 262);
            this.Controls.Add(this.BT2);
            this.Controls.Add(this.TEX5);
            this.Controls.Add(this.TEX6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EMPLEADOS_POR_COMISION";
            this.Text = "EMPLEADOS_POR_COMISION";
            this.Load += new System.EventHandler(this.EMPLEADOS_POR_COMISION_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BT2;
        private System.Windows.Forms.TextBox TEX5;
        private System.Windows.Forms.TextBox TEX6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}