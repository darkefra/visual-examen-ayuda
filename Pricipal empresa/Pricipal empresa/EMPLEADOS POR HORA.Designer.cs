﻿namespace Pricipal_empresa
{
    partial class EMPLEADOS_POR_HORA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.BT2 = new System.Windows.Forms.Button();
            this.TEX3 = new System.Windows.Forms.TextBox();
            this.TEX4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "VENTANA EMPLEADOS POR HORA";
            // 
            // BT2
            // 
            this.BT2.Location = new System.Drawing.Point(75, 176);
            this.BT2.Name = "BT2";
            this.BT2.Size = new System.Drawing.Size(144, 23);
            this.BT2.TabIndex = 10;
            this.BT2.Text = "Calcular paga";
            this.BT2.UseVisualStyleBackColor = true;
            this.BT2.Click += new System.EventHandler(this.BT2_Click);
            // 
            // TEX3
            // 
            this.TEX3.Location = new System.Drawing.Point(167, 65);
            this.TEX3.Name = "TEX3";
            this.TEX3.Size = new System.Drawing.Size(100, 20);
            this.TEX3.TabIndex = 9;
            // 
            // TEX4
            // 
            this.TEX4.Location = new System.Drawing.Point(167, 124);
            this.TEX4.Name = "TEX4";
            this.TEX4.Size = new System.Drawing.Size(100, 20);
            this.TEX4.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Horas trabajadas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Saldo por hora";
            // 
            // EMPLEADOS_POR_HORA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.BT2);
            this.Controls.Add(this.TEX3);
            this.Controls.Add(this.TEX4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EMPLEADOS_POR_HORA";
            this.Text = "EMPLEADOS_POR_HORA";
            this.Load += new System.EventHandler(this.EMPLEADOS_POR_HORA_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BT2;
        private System.Windows.Forms.TextBox TEX3;
        private System.Windows.Forms.TextBox TEX4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
    }
}